# Author Jordane Plummer 1403184
# We can accept three numbers (5,4 and 3) using the sample execution below
# ./program.sh 5 3 4

# Store the 1st argument (eg. 5) in NUM1 and the second argument (eg. 3) in NUM2 and the third argument (eg. 4)
NUM1=$1
NUM2=$2
NUM3=$3

# Check if the variable (environment) has been defined
if [ -z ${OUTPUT_FILE_NAME} ] # Returns True if variable OUTPUT_FILE_NAME is NOT defined
then
    # Define variable with default value
    OUTPUT_FILE_NAME="myresult.txt"
    echo "Using default output file name of '$OUTPUT_FILE_NAME'."
else
    echo "Using given file name of '$OUTPUT_FILE_NAME'"
fi



echo "We will attempt to add (NUM1 ^ NUM2) + NUM3 X NUM2"

# Create (overwrites if it already exists) new file with output text
echo "We have done too much work" > $OUTPUT_FILE_NAME
#@echo off
#@echo>"/root/Documents/build/$OUTPUT_FILE_NAME"

# Calculate result
RESULT=$(((NUM1**NUM2) + (NUM3 * NUM2)))

RESULT_OUTPUT="CALCULATION : $RESULT"

echo $RESULT_OUTPUT

# Append result output to file
#@echo off
#@echo $RESULT_OUTPUT  >> "/root/Documents/build/$OUTPUT_FILE_NAME"
echo $RESULT_OUTPUT >> $OUTPUT_FILE_NAME

